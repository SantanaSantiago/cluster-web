<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Clustercito;
use yii\base\UserException;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $model = new Clustercito();
        $output = [];

        if($request->isPost) {
            if(!$model->load($_POST) || !$model->validate()) {
                yii::error(VarDumper::dumpAsString($model->errors));
                return $this->render('index', compact('model'));
            }
            
            $model->file = UploadedFile::getInstance($model, 'file');
            yii::error(VarDumper::dumpAsString($model->file));
            $model->upload();
            
            $filePath = "/home/santiago/git/cluster-web/web/programas/{$model->file->name}";
            $execPath = "/home/santiago/git/cluster-web/web/programas/{$model->file->baseName}";
            
            $compileCommand = "gcc $filePath -o $execPath"; // mpicc

            exec($compileCommand, $output, $retval);
            if($retval !== 0) {
                throw new UserException("No se pudo compilar {$model->file->name}. Error: $retval");
            }

            exec("chmod +x $execPath"); // Ya que estamos haciendo animaladas...
            
            // TODO: mpiexec -n $model->procesadores -f MACHINEFILE $execPath
            $execCommand = "$execPath";
            exec("$execCommand 2>&1", $output);
            
            /**
             * Verificar que esté activado 'safe mode'.
             * No olvidar que estoy usando mi máquina de servidor para esta locura.
             * Opción más simple sería tirar el archivo por ssh a cluster-02 y ejecutar todo ahí.
             */
        }

        return $this->render('index', compact('model', 'output'));
    }
}
