<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class Clustercito extends Model
{
    /** @var int */
    public $procesadores;

    /** @var UploadedFile */
    public $file;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['procesadores'], 'required'],
            ['procesadores', 'integer', 'min' => 1, 'max' => '4'],
            ['file', 'file', 'skipOnEmpty' => true, 'extensions' => 'c txt', 'maxSize' => 1024 * 512],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'procesadores' => 'Cantidad de Procesadores',
            'file' => 'Programa',
        ];
    }

    public function upload()
    {
        if(!$this->validate()) {
            return false;
        }

        $path = 'programas/' . $this->file->baseName . '.' . $this->file->extension;
        return $this->file->saveAs($path);
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function contact($email)
    {
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->name])
                ->setSubject($this->subject)
                ->setTextBody($this->body)
                ->send();

            return true;
        }
        return false;
    }
}
