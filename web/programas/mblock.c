#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define N 12 // multiplo de 12 (2x2, 3x3, 4x4)

int main(int argc, char** argv)
{
    int i, j;
    int A[N][N];
    int sum=0;

    srand(time(NULL));
    
    for(i=0; i<N; i++) {
        for(j=0; j<N; j++) {
            A[i][j] = rand() % 20;
        }
    }

    for(int k=0; k<N; k++) {
        for(int l=0; l<N; l++) {
            printf("%d\t", A[k][l]);
        }
        printf("\n");
    }
    
    for(i=0; i<N; i++) {
        for(j=0; j<N; j++) {
            sum = (sum + A[i][j]) % 10;
        }
    }

    printf("\n suma de A %% 10 = %d\n", sum);
    
    return 0;
} /* main */
