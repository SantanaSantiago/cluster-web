#include <stdio.h>
#include <mpi.h>
int main(int argc, char** argv)
{
	int pid, npr;
	int i, X;
	int A[8], B[2], C[2];
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &pid);
	MPI_Comm_size(MPI_COMM_WORLD, &npr);
	
	/* Inicializamos 3 vectores en -1
	 * El primer vector tiene 8 elementos
	 * (de 0 a 7) y los otros dos vectores
	 * tienen 2 elementos
	 */
	for(i=0; i<8; i++) A[i] = -1;
	for(i=0; i<2; i++) B[i] = -1;
	for(i=0; i<2; i++) C[i] = -1;
	
	X = -1; // Inicializamos X en -1

	/* Solamente en el proceso cero:
	 * Le damos a cada elemento del vector A
	 * el valor de su posición
	 * e inicializamos X en 6
	 * P0 : A = [0, 1, 2, 3, 4, 5, 6, 7]
	 * P1 : A = [-1, -1, -1, -1, -1, -1, -1, -1]
	 * P2 : A = [-1, -1, -1, -1, -1, -1, -1, -1]
	 * P3 : A = [-1, -1, -1, -1, -1, -1, -1, -1]
	 */
	if (pid == 0)
	{ 
		for(i=0; i<8; i++)
			A[i] = i;
		X = 6;
	}

	/* Broadcast de X desde P0,
	 * ahora X vale 6 en todos los procesos
	 * la inicialización anterior en -1 fue al pedo.
	 */
	MPI_Bcast(&X, 1, MPI_INT, 0, MPI_COMM_WORLD);

	/* Imprimimos el resultado de hacer el broadcast
	 * a cada proceso y ponemos una barrera para
	 * asegurarnos de que el valor de X llegué a todos los 
	 * procesos antes de continuar.
	 */
	printf("1: BC de P0 --> P%d y X = %d \n", pid, X);
	MPI_Barrier(MPI_COMM_WORLD);
	
	/* Este programa esta preparado para ser ejecutado en
	 * 4 procesos, de forma que Scatter divida el vector A
	 * de 8 elementos en 4 vectores de dos elementos, cada
	 * vector en un proceso diferente.
	 *
	 * El resultado sería entonces:
	 * P0 : B = [0, 1]
	 * P1 : B = [2, 3]
	 * P2 : B = [4, 5]
	 * P3 : B = [6, 7]
	 *
	 * Luego ponemos una barrera para asegurarnos de que 
	 * los vectores lleguen a destino antes de continuar.
	 */
	MPI_Scatter(&A[0], 2, MPI_INT, &B[0], 2, MPI_INT, 0, MPI_COMM_WORLD);
	printf(" 2: SCATTER soy P%d y B = %d %d \n", pid, B[0], B[1]);
	MPI_Barrier(MPI_COMM_WORLD);
	
	/* Reduce permite juntar con una función los vectores de cada proceso
	 * en un proceso.
	 *
	 * En este caso junta todos los vectores B de dos elementos
	 * con la función suma en el vector C del proceso cero.
	 *
	 * Esto significa que C[0] _en el proceso cero_ va a tener la suma de 
	 * B[0] en todos los procesos, y C[1] la suma de todos los B[1].
	 * 
	 * Entonces luego de ejecutar el comando reduce de abajo quedaría:
	 * P0 : C = [12, 16]
	 * P1 : C = [-1, -1]
	 * P2 : C = [-1, -1]
	 * P3 : C = [-1, -1]
	 *
	 * Luego una barrera para asegurarnos de que todos los procesos terminen el Reduce.
	 */
	MPI_Reduce(&B[0], &C[0], 2, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	printf(" 3: REDUCE en P%d C = %d %d \n", pid, C[0], C[1]);
	MPI_Barrier(MPI_COMM_WORLD);
	
	/* Allreduce hace lo mismo que Reduce pero distribuye el resultado a todos los procesos.
	 * En resumen si partimos de los mismos B que en el punto anterior, entonces:
	 * P0 : C = [12, 16]
	 * P1 : C = [12, 16]
	 * P2 : C = [12, 16]
	 * P3 : C = [12, 16]
	 */ 
	MPI_Allreduce(&B[0], &C[0], 2, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
	printf(" 4. ALLREDUCE en P%d C = %d %d \n", pid, C[0], C[1]);
	MPI_Barrier(MPI_COMM_WORLD);
	
	// Sumo 10 a cada valor del vector B en todos los procesos
	for(i=0; i<2; i++)
		B[i] = B[i] + 10;
	
	/* Junto los resultados de todos los B en el vector A del proceso cero
	 * 
	 * P0 : A = [10, 12, 13, 14, 15, 16, 17, 18]
	 * P1 : A = [-1, -1, -1, -1, -1, -1, -1, -1]
	 * P2 : A = [-1, -1, -1, -1, -1, -1, -1, -1]
	 * P3 : A = [-1, -1, -1, -1, -1, -1, -1, -1]
	 */
	MPI_Gather(&B[0], 2, MPI_INT, &A[0], 2, MPI_INT, 0, MPI_COMM_WORLD);
	
	printf(" 5: GATHER en P%d, A = %d %d %d %d %d %d %d %d\n", 
			pid, A[0], A[1], A[2], A[3], A[4], A[5], A[6], A[7]);
	MPI_Barrier(MPI_COMM_WORLD);
	
	/* Al igual que Allreduce distribuye los resultados del reduce
	 * Allgather es como gather pero distribuyendo los resultados
	 *
	 * P0 : A = [10, 12, 13, 14, 15, 16, 17, 18]
	 * P1 : A = [10, 12, 13, 14, 15, 16, 17, 18]
	 * P2 : A = [10, 12, 13, 14, 15, 16, 17, 18]
	 * P3 : A = [10, 12, 13, 14, 15, 16, 17, 18]
	 */
	MPI_Allgather(&B[0], 2, MPI_INT, &A[0], 2, MPI_INT, MPI_COMM_WORLD);
	printf(" 6: ALLGATHER en P%d, A = %d %d %d %d %d %d %d %d\n", pid,
	A[0],A[1],A[2],A[3],A[4],A[5],A[6],A[7]);
	
	MPI_Finalize();
	return 0;
}
