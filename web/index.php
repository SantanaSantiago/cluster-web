<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require '/home/santiago/git/cluster-web/vendor/autoload.php';
require '/home/santiago/git/cluster-web/vendor/yiisoft/yii2/Yii.php';

$config = require '/home/santiago/git/cluster-web/config/web.php';

(new yii\web\Application($config))->run();
