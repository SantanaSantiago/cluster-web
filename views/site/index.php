<?php

use \app\models\Clustercito;
use \yii\bootstrap\ActiveForm;
use \yii\bootstrap\Html;
use yii\bootstrap\Alert;

/* @var $this yii\web\View */

$this->title = 'Clustercito';

$this->params['breadcrumbs'][] = '';

?>
<?= Alert::widget([
    'options' => [
        'class' => 'alert-info',
    ],
    'body' => implode("<br>", $output),
]); ?>

<div class="site-index">
    <div class="body-content">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <p class="panel-title">Cargar Programa</p>
                    </div>
                    <div class="panel-body">
                        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
                            <?= $form->field($model, 'procesadores')->textInput([
                                'type' => 'number',
                                'min' => 1,
                                'max' => 4,
                                'value' => 1,
                            ]) ?>
                            <?= $form->field($model, 'file')->fileInput() ?>
                            <div class="form-group pull-right">
                                <?= Html::submitButton('Compilar y Ejecutar', ['class' => 'btn btn-primary']) ?>
                           </div>
                        <?php ActiveForm::end() ?>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
